# Assessment Project - HTML, CSS, AngularJS

Welcome to the Tipico Front-End Assessment Project - HTML, CSS, AngularJS

Here you find a short summary of your tasks. Please use this project "fe_assessment" as your skeleton and add new sources to complete the tasks. 
Follow the **Development and Submission steps** in order to do the below **Tasks** and submit them to us for review.

## Tasks (Minimum Requirements)

Create a small mobile app where users are able to:

1. View live bets as shown in the image tipico/live/livetopgame.png (implementation of the live view and statistics tabs are not required)
2. Live pitch events can be found at img/live, and upon refresh + a change in the JSON response the user is expected to see the respective live icon. 
3. Kindly use the following JSON as a mocked response from server: http://promo.tipico-news.com/fe-assesment/live-event-service-min.js
4. Live markets in the design are for reference only, kindly refer to the JSON response for the actual live markets.
5. Odds are clickable, and upon selecting, they are expected to be added to the betslip.
6. Betslip will by default be empty - no events. Betslip designs can be found in  tipico/betslip. The Possible gain calculation is ((total odds * total stake) - total stake).
7. Odds are to be static (no-backend functionality) and clickable, being added to the betslip once added
8. If only one event is added to the betslip, SINGLE tab should be selected
9. SYSTEM + COMBI tabs within the betslip are not required in this assessment
10. Proper client-side error handling + validations
11. Ability to delete bets from the betslip.
12. 'Place Bet' will display a message that the betslip has been submitted
13. Betslip is accessible from the header (top right corner) and/or swipe
14. Header text showing account info should be hardcoded as a demo. 
15. Footer of the Live Event page is not required.


## Using the mock data service

We also provided a service which simulates an asynchronous call to a server or api. It is actually an angular
service, returning mock data and selecting a random last action which happened in the game.


#### Using the third party angular service
First, include the third party module in your application, located here:

<pre><code>
&lt;script type="text/javascript" src="http://promo.tipico-news.com/fe-assesment/live-event-service-min.js"&gt;&lt;/script&gt;
</code></pre>


And reference it in your angular module:
<pre><code>
angular.module('yourApp', ['live.event.service']);
</code></pre>

To make use of this service simply inject the dependency in your angular component (service, controller, directive etc etc)

<pre><code>
yourApp.controller('YourController', function (..., liveEventService) {
  //...
</code></pre>


This service offers two method calls. 
* getEventDetails
* getEventLastAction


The first returns the full event details object which includes the lastAction json property and can be used as follows:

<pre><code>
liveEventService.getEventDetails().then(function(eventDetailsData){
	//todo - your logic here
});
</code></pre>

It returns data structured as follows:

<pre><code>
{
    "id": 148122710,
    "title": "Bayern Munich - Borussia Dortmund",
    "sport": "Football",
    "country": "Germany",
    "version": "25fd0a7d5651a7a5cac1f10e5547b549a1bd59531cc4e866b6c91f31bc2b5fca",
    "odds": [
      {
        "displayName": "3-Way",
        "results": [
          {
            "id": 16646752110,
            "name": "1",
            "value": "1,01",
            "status": null,
            "selected": false
          },
          //... more market result types
        ]
      },
      //...more odds details
    ],
    "bettingAvailable": true,
    "team1": "Bayern Munich",
    "team2": "Borrusia Dortmund",
    "ended": false,
    "livescore": {
      "team1": "3",
      "team2": "1",
      "scoreString": "3:1"
    },
    "liveevent": {
      "action": "goalkick",
      "team": 1
    },
    "matchtime": "38'",
    "lastAction": {
      //populated randomly each time the service is called
    }
}
</code></pre>

The second function returns the lastAction that happened in this event and can be used as follows:

<code><pre>
liveEventService.getEventLastAction().then(function(lastAction){
	//todo - your logic here
});
</code></pre>

The data returns should look like this:

<code><pre>
{
	"actionType" : "Goal Kick",
	"team" : 1
}
</code></pre>


## Development and Submission steps

1. Clone the master branch to your local machine
2. Please commit frequently to your local git repository (no need to push)
3. When ready, make sure everything is committed
4. Add any assumptions or notes to the file named COMMENTS.md
5. Compress your solution in ZIP format, upload to a cloud service and send us the link for us to be able to download the solution.


**Good luck!**